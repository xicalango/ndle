# ndle

wordle but with n words to guess.

Play here:

- [1-dle](https://xicalango.gitlab.io/ndle/?n=1)
- [2-dle](https://xicalango.gitlab.io/ndle/?n=2)
- [4-dle](https://xicalango.gitlab.io/ndle/?n=4)
- [8-dle](https://xicalango.gitlab.io/ndle/?n=8)
- [16-dle](https://xicalango.gitlab.io/ndle/?n=16)
- [32-dle](https://xicalango.gitlab.io/ndle/?n=32)
- [64-dle](https://xicalango.gitlab.io/ndle/?n=64)
- [128-dle](https://xicalango.gitlab.io/ndle/?n=128)

Supports non-powers of two, too:

- [3-dle](https://xicalango.gitlab.io/ndle/?n=3)
- [6-dle](https://xicalango.gitlab.io/ndle/?n=6)
- [13-dle](https://xicalango.gitlab.io/ndle/?n=13)

# Query parameters

- `?n=<n>` - replace `<n>` with the number of words you want to guess
- `?w=<w>` - replace `<w>` with the number of words per row
- `?t` - training mode (not daily mode)

# Credits

- Inspiration and list of words: [wordle](https://www.nytimes.com/games/wordle/index.html)
- StackOverflow
  - [Seeded random number generator](https://stackoverflow.com/a/47593316)
  - [Array permutations](https://stackoverflow.com/a/15961211)
