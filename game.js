const N_COLS = 5;
const N_EXTRA = 5;

const BACKSPACE_KEY = '⌫';
const RETURN_KEY = '⏎';

const LAUNCH_DATE = new Date(2022, 2, 19);

class States {

  static Guessing = new States("Guessing");
  static FullRowGood = new States("FullRowGood");
  static FullRowBad = new States("FullRowBad");
  static EndWon = new States("EndWon");
  static EndLost = new States("EndLost");

  constructor(name) {
    this.name = name;
  }

  is_end() {
    return this == States.EndWon || this == States.EndLost;
  }

  is_full() {
    return this == States.FullRowBad || this == States.FullRowGood;
  }

  is_running() {
    return !this.is_end();
  }

  can_guess() {
    return this.is_running() && !this.is_full();
  }

}

const LetterStates = {
  Incorrect: 0,
  HalfCorrect: 1,
  FullCorrect: 2
}

class GameState {
  constructor(ncols, words, valid_words, num_extra_guesses) {
    this.ncols = ncols;
    this.nrows = words.length + num_extra_guesses;
    this.words = words;
    this.valid_words = new Set(valid_words);
    this.col = 0;
    this.row = 0;
    this.guesses = [];
    this.guess_states = [];
    this.solved_state = Array(words.length).fill(false);
    this.current_guess = this._create_new_current_guess();
    this.state = States.Guessing;
  }

  _create_new_current_guess() {
    return Array(this.ncols).fill('');
  }

  _validate_guess(word, guess) {
    let validation = Array(word.length).fill(LetterStates.Incorrect);

    let letter_map = new Map();

    for (let i = 0; i < word.length; i++) {
      let letter = word.charAt(i);

      if (guess[i] == letter) {
        validation[i] = LetterStates.FullCorrect;
        continue;
      }

      if (!letter_map.has(letter)) {
        letter_map.set(letter, []);
      }
      letter_map.get(letter).push(i);
    }

    guess.forEach( (letter, index) => {
      if (validation[index] == LetterStates.FullCorrect) {
        return;
      }

      if (!letter_map.has(letter)) {
        return;
      }

      validation[index] = (LetterStates.HalfCorrect);

      let letter_info = letter_map.get(letter);
      letter_info.shift();
      if (letter_info.length == 0) {
        letter_map.delete(letter);
      }

    });

    return validation;
  }

  _validate_all_guesses(guess) {
    return this.words.map( (word) => {
      let validation = this._validate_guess(word, guess);
      return {
        "validation": validation,
        full_correct: validation.every((entry) => entry == LetterStates.FullCorrect)
      };

    });
  }

  guess_letter(letter) {

    if (this.state.is_end()) {
      return;
    }

    if (letter == BACKSPACE_KEY) {
      if (this.col == 0) {
        return;
      }

      this.col -= 1;
      this.current_guess[this.col] = '';
      this.state = States.Guessing;
      return;
    }

    if (letter == RETURN_KEY) {

      if (this.state == States.FullRowBad) {
        let cur_guess = this.current_guess.join('');
        if (cur_guess == "xyzzy") {
          this.state = States.EndWon;
        } else if (cur_guess == "yzzyx") {
          this.state = States.EndLost;
        }
        return;
      }

      if (this.state != States.FullRowGood) {
        return;
      }

      // make guess
      let validations = this._validate_all_guesses(this.current_guess);
      validations.forEach( (validation, index) => {
        this.solved_state[index] |= validation.full_correct
      });
      this.guess_states.push(validations);
      this.guesses.push(this.current_guess);
      this.row += 1;
      this.current_guess = this._create_new_current_guess();
      this.col = 0;

      if (this.solved_state.every(state => state)) {
        this.state = States.EndWon;
      } else if (this.row >= this.nrows) {
        this.state = States.EndLost;
      } else {
        this.state = States.Guessing;
      }

      return;
    }

    if (!this.state.can_guess()) {
      return;
    }

    this.current_guess[this.col] = letter;
    this.col += 1;

    if (this.col == this.ncols) {
      let guessword = this.current_guess.join('');
      if (!this.valid_words.has(guessword)) {
        this.state = States.FullRowBad;
      } else {
        this.state = States.FullRowGood;
      }
    } else {
      this.state = States.Guessing;
    }

  }

}

class ViewState {
  constructor(ncols, nrows, num_ndles, ndles_per_row = 2, training_mode) {
    this.ncols = ncols;
    this.nrows = nrows;
    this.num_ndles = num_ndles;
    this.tables = [];
    this.ndles_per_row = ndles_per_row;
    this.current_row = 0;
    this.done_tables = new Set();
    this.training_mode = training_mode
  }

  init(target) {

    var current_tr = null;

    target.innerHtml = '';

    for (var i = 0; i < this.num_ndles; i++) {

      if (i % this.ndles_per_row == 0) {
        if (current_tr != null) {
          let spacer_tr = document.createElement("tr");
          let spacer_td = document.createElement("td");
          spacer_td.classList.add("spacer");
          spacer_tr.appendChild(spacer_td);
          target.appendChild(spacer_tr);
        }

        current_tr = document.createElement("tr");
        target.appendChild(current_tr);
      }

      const table = this._generate_guessbox(i);
      this.tables.push(table);
      const current_td = document.createElement("td");
      current_td.appendChild(table);
      current_tr.appendChild(current_td);
    }

  }

  _generate_guessbox(name) {
    const table = document.createElement("table");
    table.id = "table_" + name;

    for (var y = 0; y < this.nrows; y++) {
      const row_tr = document.createElement("tr");
      row_tr.id = "row_" + name + "," + y;

      for (var x = 0; x < this.ncols; x++) {
        const row_td = document.createElement("td");
        row_td.id = this._get_cell_id(name, y, x);
        row_td.classList.add("box");
        row_tr.appendChild(row_td);
      }

      table.appendChild(row_tr);
    }

    return table;
  }

  _get_cell_id(table_index, row, col) {
    return `cell_${table_index},${row},${col}`;
  }

  _get_cell(table_index, row, col) {
    return document.getElementById(this._get_cell_id(table_index, row, col));
  }

  sync_gamestate(state) {
    console.log(state);

    if (state.row != this.current_row) {
      let guess_index = this.current_row;
      let guess = state.guesses[guess_index];

      for (var i = 0; i < this.num_ndles; i++) {

        if (this.done_tables.has(i)) {
          continue;
        }

        let validation = state.guess_states[guess_index][i];
        if (validation.full_correct) {
          this.done_tables.add(i);
        }

        guess.forEach((letter, letter_index) => {
          const cell = this._get_cell(i, guess_index, letter_index);
          cell.textContent = letter.toUpperCase();

          let letter_validation = validation.validation[letter_index];
          if (letter_validation == LetterStates.FullCorrect) {
            cell.classList.add("full_correct");
          } else if (letter_validation == LetterStates.HalfCorrect) {
            cell.classList.add("half_correct");
          }
        });

      }

      this.current_row = state.row;
    }

    if (state.state.is_running()) {
      state.current_guess.forEach((letter, letter_index) => {
        for (var i = 0; i < this.num_ndles; i++) {
          if (this.done_tables.has(i)) {
            continue;
          }

          const cell = this._get_cell(i, state.row, letter_index);
          cell.textContent = letter.toUpperCase();
          if (state.state == States.FullRowBad) {
            cell.classList.add("wrong_word");
          } else {
            cell.classList.remove("wrong_word");
          }
        }
      });
    } else {

      let message_div = document.getElementById("message");
      let message_content_div = document.getElementById("message_content");
      message_div.style.display = "block";

      if (state.state == States.EndWon) {
        message_content_div.textContent = "Good game!";
        message_div.classList.add("good_game");
        message_div.classList.remove("wrong_word");
      } else {
        let words = state.words.join(", ");
        message_content_div.textContent = `Too bad! The words are: ${words}.`;
        message_div.classList.remove("good_game");
        message_div.classList.add("wrong_word");
      }

      let results_text = [];

      let date = this.training_mode ? "training" : new Date().toISOString().split('T')[0];

      results_text.push(`${this.num_ndles}-dle ${date}\n\n`);

      let results_details = [];

      let num_correct = 0;

      for (let i = 0; i < this.num_ndles; i++) {
        let num_guesses = 1;
        for (let guess_id = 0; guess_id < state.guess_states.length; guess_id++) {
          if (state.guess_states[guess_id][i].full_correct) {
            num_correct += 1;
            break;
          }
          num_guesses += 1;
        }

        if (num_guesses <= this.nrows) {
          results_details.push(`${num_guesses}`);
        } else {
          results_details.push("🟥");
        }

      }

      results_text.push(`${num_correct}/${this.num_ndles}\n\n`);

      let actual_per_row = Math.min(this.num_ndles, this.ndles_per_row);

      let full_rows = Math.floor(this.num_ndles / actual_per_row);

      for (let i = 0; i < full_rows * actual_per_row; i++) {
        results_text.push(results_details[i]);
        if (i % actual_per_row < actual_per_row - 1) {
          results_text.push("-");
        } else {
          results_text.push("\n");
        }
      }

      for (let i = full_rows * actual_per_row; i < this.num_ndles; i++) {
        results_text.push(results_details[i]);
        if (i < this.num_ndles - 1) {
          results_text.push("-");
        } else {
          results_text.push("\n");
        }
      }

      document.getElementById("results_area").value = results_text.join('');

    }

  }

}

class Rng {
  constructor(seed) {
    let seed_fn = this._xmur3(seed);
    this.a = seed_fn();
    this.b = seed_fn();
    this.c = seed_fn();
    this.d = seed_fn();
  }

  _xmur3(str) {
    for (var i = 0, h = 1779033703 ^ str.length; i < str.length; i++) {
      h = Math.imul(h ^ str.charCodeAt(i), 3432918353);
      h = h << 13 | h >>> 19;
    } return function () {
      h = Math.imul(h ^ (h >>> 16), 2246822507);
      h = Math.imul(h ^ (h >>> 13), 3266489909);
      return (h ^= h >>> 16) >>> 0;
    }
  }

  next() {
    this.a >>>= 0; this.b >>>= 0; this.c >>>= 0; this.d >>>= 0;
    var t = (this.a + this.b) | 0;
    this.a = this.b ^ this.b >>> 9;
    this.b = this.c + (this.c << 3) | 0;
    this.c = (this.c << 21 | this.c >>> 11);
    this.d = this.d + 1 | 0;
    t = t + this.d | 0;
    this.c = this.c + t | 0;
    return t >>> 0;
  }

  permutate_inplace(array) {
    for(let i = array.length - 1; i >= 0; i--) {
      let pos = this.next() % (i+1);

      let tmp = array[i];
      array[i] = array[pos];
      array[pos] = tmp;
    }
  }

}

function generate_keyboard(keyboard_table, callback) {
  const rows = ["qwertyuiop", "asdfghjkl", "⌫zxcvbnm⏎"];

  rows.forEach((row, index) => {

    const row_tr = document.createElement("tr");
    row_tr.id = "keyboard" + index;

    for (var i = 0; i < row.length; i++) {
      const letter = row.charAt(i);

      const row_td = document.createElement("td");
      row_td.id = "keyboard" + index + "_" + letter;
      row_td.classList.add("box");
      row_td.classList.add("button");
      row_td.onclick = () => callback(letter);
      row_td.appendChild(document.createTextNode(letter.toUpperCase()));
      row_tr.appendChild(row_td);
    }

    keyboard_table.appendChild(row_tr);
  });

}

(function init() {

  const urlParams = new URLSearchParams(window.location.search);

  let nwords = urlParams.get('n');

  if (nwords == null || nwords.length == 0) {
    nwords = "1";
  }

  let width = urlParams.get('w');

  if (width == null || width.length == 0) {
    width = "8";
  }

  let num_words = parseInt(nwords);
  let games_width = parseInt(width);
  let rng = new Rng(num_words.toString());

  rng.permutate_inplace(NDLE_WORDS.solutions);

  let my_words = [];

  let today = new Date();
  let diffTime = today - LAUNCH_DATE;
  var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

  if (urlParams.has('t')) {
    let training_rng = new Rng(diffTime.toString());
    let rnd_num = training_rng.next();
    diffDays = rnd_num % NDLE_WORDS.solutions.length;
  }

  let offset = (diffDays * num_words);
  for (let i = 0; i < num_words; i++) {
    let index = (i + offset) % NDLE_WORDS.solutions.length;
    my_words.push(NDLE_WORDS.solutions[index]);
  }

  const game_state = new GameState(N_COLS, my_words, NDLE_WORDS.words.concat(NDLE_WORDS.solutions), N_EXTRA);
  const view_state = new ViewState(N_COLS, game_state.nrows, game_state.words.length, games_width, urlParams.has('t'));

  function guess_letter(letter) {
    const valid_keys = "qwertyuiopasdfghjklzxcvbnm" + RETURN_KEY + BACKSPACE_KEY;
    if (!valid_keys.includes(letter)) {
      return;
    }
    game_state.guess_letter(letter);
    view_state.sync_gamestate(game_state);
  };

  function press_key(event) {
    if (event.key == 'Enter') {
      guess_letter(RETURN_KEY);
    } else if (event.key == 'Backspace') {
      guess_letter(BACKSPACE_KEY);
    } else {
      guess_letter(event.key);
    }
  }

  const keyboard_table = document.getElementById("keyboard");
  generate_keyboard(keyboard_table, (letter) => guess_letter(letter));

  const ndles_table = document.getElementById("ndles");
  const message_div = document.getElementById("message");
  view_state.init(ndles_table, message_div);

  document.addEventListener('keydown', press_key);
  
  document.getElementById("share_results").onclick = () => {
    let results_area = document.getElementById("results_area");
    navigator.clipboard.writeText(results_area.value);
  };
})();
